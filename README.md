# Simple
A Simple Nginx project on deployed on k8s with GitOps workflow using Gitlab Kubernetes Agent (AgentK)

## To run this project
You will need
1. A K8s Cluster with Kubernetes Agent Server Running ([KAS](https://docs.gitlab.com/ee/administration/clusters/kas.html))
2. [Nginx Ingress Controller](https://kubernetes.github.io/ingress-nginx/deploy/) installed in your cluster
3. [Register](https://kubernetes.github.io/ingress-nginx/deploy/) the AgentK with your KAS